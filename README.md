<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Orchestrator Flows

A flow describe logical connection of endpoints, actions and outputs. It is managed as independend Node-Red project and merged together into orchestrator-flows after deployment.

### About
It is the major configuration for the orchestrator project. It hold all the sub project flows.

### Notice
All changes must be merged into [main] to be pulled by [Orchestrator Service](https://gitlab.com/ambtec/orchestrator-service)